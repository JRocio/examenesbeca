﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ABARROTES_CORE.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace ABARROTES_CORE.Controllers
{
    public class MyProductController : Controller
    {  
        private readonly ApplicationDbContext db;
        public MyProductController (ApplicationDbContext d_b)
        {
            db = d_b;
        }

        // GET: MyProductController
        public ActionResult Index()
        {
            var displaydata = db.Product.ToList();
            return View(displaydata);
        }
        [HttpGet]
        public async Task<IActionResult> Index(string ProdSearch)
        {
            ViewData["GetProduct"] = ProdSearch;
            var prquery = from x in db.Product select x;
            if (!String.IsNullOrEmpty(ProdSearch))
            {
                prquery = prquery.Where(x => x.Prodname.Contains(ProdSearch));
            }
            return View(await prquery.AsNoTracking().ToListAsync());
        }
        // GET: MyProductController/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if(id==null)
            {
                return RedirectToAction("Index");
            }
            var getProdDetail = await db.Product.FindAsync(id);
            return View(getProdDetail);
        }
        
        // GET: MyProductController/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: MyProductController/Create
        [HttpPost]
        public async Task<IActionResult> Create(Product newPr)
        {
            if(ModelState.IsValid)
            {
                db.Add(newPr);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(newPr);
           
        }

        // GET: MyProductController/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getProdDetail = await db.Product.FindAsync(id);
            return View(getProdDetail);
        }

        // POST: MyProductController/Edit/5
        [HttpPost]
        public async Task<IActionResult> Edit(Product oldP)
        {
            if (ModelState.IsValid)
            {
                db.Update(oldP);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(oldP);
        }

        // GET: MyProductController/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            var getProdDetail = await db.Product.FindAsync(id);
            return View(getProdDetail);
        }

        // POST: MyProductController/Delete/5
        [HttpPost]
        public async Task<IActionResult> Delete(int id)
        {

            var getProdDetail = await db.Product.FindAsync(id);
            db.Product.Remove(getProdDetail);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");

        }
    }
}
