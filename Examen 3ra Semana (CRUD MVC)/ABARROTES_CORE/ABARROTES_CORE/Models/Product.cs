﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ABARROTES_CORE.Models
{
    public class Product
    {
        [Key]
        [Display(Name = "ID")]
        public int Prodid { get; set; }

        [Required(ErrorMessage = "Introduce nombre del producto")]
        [Display(Name = "Producto")]
        public String Prodname { get; set; }

        [Required(ErrorMessage = "Introduce el departamento")]
        [Display(Name = "Departamento")]
        public String Depar { get; set; }

        [Required(ErrorMessage = "Introduce el precio del producto")]
        [Display(Name = "Precio")]
        [Range(1, 1000)]
        public int Price { get; set; }

        [Display(Name = "Descuento disponible ")]
        public String Disc { get; set; }

    }
}
