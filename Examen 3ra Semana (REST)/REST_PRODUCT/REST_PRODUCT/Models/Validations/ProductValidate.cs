﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace REST_PRODUCT.Models
{
    [MetadataType(typeof(Product.Metadata))]
    public partial class Product
    {
        sealed class Metadata
        {
            [Key]
            public int Prodid;

            [Required(ErrorMessage ="Ingresar nombre del producto")]
            public string Prodname;

            [Required(ErrorMessage = "Ingresar departamento del producto")]
            public string Depar;

            [Required(ErrorMessage = "Ingresar precio del producto")]
            [Range(1,500)]
            public Nullable<int> Price;

            public string Disc;
        }
    }
}